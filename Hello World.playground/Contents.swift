import UIKit

var str = "Hello, playground";
print(str)

let firstName = "Aldhaneka";
let middleName = "Aufa";
let lastName = "Izzat";

let fullName = firstName + middleName + lastName;

print(fullName)

// ====================================================================================================================================
// Rules naming variables
/*
 
    * Names can’t contain mathematical symbols.
    * Names can’t contain spaces.
    * Names can’t begin with a number
 
    Excerpt From: Apple Education. “Develop in Swift Fundamentals.” Apple Inc. - Education, 2020. Apple Books.
 
    Example:
    
    let  = "b";
    let 3124134 = "hello!";
    let = = "hey!"
 
*/


let π = 3.14159
let 一百 = 100
let 🎲 = 6
let mañana = "Tomorrow"
let anzahlDerBücher = 15 // numberOfBooks”

print(π)
print(type(of: π))

// swift is a type safty programming language, you can't assign variable to another variable that has different type
// for example
let pi = 3.14;
var pai = 3;
//pai = pi; will throws an error

// Swift is type inference programming language, so you don't need specify the type of the variable
// Swift is type annotation programming language, so you can spcify the type of the variable
/*
 
 let b: Int = 3;
 print(b) // prints 3.0
 
 */

/*
 There are three common cases for using type annotation:
When you create a constant or variable but haven’t yet assigned it a value.
 
let firstName: String
//...
firstName = “Layne”
 
When you create a constant or variable that could be inferred as more than one type.
 
let middleInitial: Character = “J”
// “J” would be inferred as a `String`, but we want a
`Character`
 
var remainingDistance: Double = 30
// `30` would be inferred as an `Int`, but the variable should
support decimal numbers for accuracy as the number decreases.
 
When you write your own type definition.
 
struct Car {
  var make: String
  var model: String
  var year: Int
}
*/

//Excerpt From: Apple Education. “Develop in Swift Fundamentals.” Apple Inc. - Education, 2020. Apple Books.

// ================================================== "Object in Swift or Dictionary" ======================================================
struct Person {
    let firstName: String
    let lastName : String
    let middleName: String
    
    func getFullName() {
        print("Hello \(firstName) \(middleName) \(lastName)")
    }
}

let aldhan = Person(firstName: "Aldhaneka", lastName: "Izzat", middleName: "Aufa")
print(aldhan.getFullName())
print(aldhan.firstName)
//print(firstName);

let thori = Person(firstName: "Muhammad", lastName: "Dzakwan", middleName: "Atthari")
//print(thori["firstName"])
//print(firstName)
//var array = ["Hello", "Hello again!"];

//for item in array {
//    print(item)
//}
//array.popLast()
//print(array)


//var BBBBB:Int;

// ================================================== "Operators" ======================================================
//var apaAja = ["Hello World!"];
//str = apaAja[0]
//print(str)
let r = 7;
let luas = π * Double(r) * Double(r);
print(luas)

// ================================================== "Control Flow or Condition Statement" =================================================

let humans = 1_000_000_000;
if humans > 5_000_000 && humans <= 1_000_000_000 {
    print("Hello Zefa")
}else {
    print("hello");
}


let character = "z";
 
switch character {
case "a", "e", "i", "o", "u":
    print("This character is a vowel.")
default:
    print("This character is a consonant.")
}

//Excerpt From: Apple Education. “Develop in Swift Fundamentals.” Apple Inc. - Education, 2020. Apple Books.

let finish = 1_000
//let start = 1;
var currentPosition = 900
var status = currentPosition == finish;
String(status)
print(type(of: status))

var largest:Int;
largest = max(finish, currentPosition);
print(largest)

// ===================================================== Xcode =========================================================================

