let thisYear:Int = 2020;
let thisMonth:String = "August";
let thisDay:String = "Saturday";

print("Hi Aldhaneka Aufa Izzat");
print("This is \(thisYear), and this month is \(thisMonth), and this day is \(thisDay)");
print("#Made by Aldhaneka");

var programingLanguageIdentity: String = "JavaScript";
var programmingCategory: String;
switch programingLanguageIdentity {
case "R", "Python", "Ruby":
    programmingCategory = "Data Science"
case "C", "C++", "C#", "JavaScript", "Java":
    programmingCategory = "Game Development"
case "Java", "JavaScript":
    programmingCategory = "Dekstop Application"
default:
    programmingCategory = "unknown"
}
print(programmingCategory)

var p:Int!
p = 123
print(p)
